---
title: 'CS 577 - Reverse Engr and Application Analy'
---

Files relating to Stevens Institute of Technology's Concurrent Programming course

Notes are hosted
[here](https://myriacore-college-work.gitlab.io/cs-577/readme.html).

## Directories

| Directory                   | Purpose / Description                                                     |
| --------------------------- | ------------------------------------------------------------------------- |
| [`notes`](/notes)           | Contains notes and work done in-lecture, including any in-class projects. |
| [`coursework`](/coursework) | Contains homework and projects done out of class.                         |
