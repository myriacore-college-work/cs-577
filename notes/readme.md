---
title: Lecture Notes
---

This directory contains a list of lecture notes I took for this course. 

| Lecture Date       | List of Topics Covered                                                                         |
| ------------------ | ---------------------------------------------------------------------------------------------- |
| [Sept 2](9-02.md)  | Structure & syllabus of course, intro to reverse engineering                                   |
| [Sept 8](9-08.md)  | Q&A, Setup REMnux Environment, How to SSH into REMnux                                          |
| [Sept 9](9-09.md)  | Basic Info about binary files                                                                  |
| [Sept 16](9-16.md) | Intro to Assembly (System-V ABI with intel syntax)                                             |
| [Sept 23](9-23)    | Went over runtime behaviors (like the stack, linking & loading, etc)                           |
| [Oct 14](10-14.md) | Discussed binary rewrite and code obfuscation techniques                                       |
| [Oct 20](10-20.md) | Went over [Homework 1](/homework/hw1), including some neat tricks in GDB.                      |
| [Oct 21](10-21.md) | Started talking about finding memory vulnerabilities in code                                   |
| [Oct 28](10-28)    | Talked about memory errors at the binary level. Demoed afl and angr for finding memory errors. |
| [Nov 18](11-18.md) | Discussed malware behaviors                                                                    |
