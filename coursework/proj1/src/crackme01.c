/* ~\~ language=C filename=src/crackme01.c */
/* ~\~ begin <<readme.md|src/crackme01.c>>[0] */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("Please provide an argument.\n");
    return 1;
  }

  /* ~\~ begin <<readme.md|crackme01-main-loop>>[0] */
  char lorem[] = "LOREMIPSUM";

  for (int i = 0; i < 10; i++) {
    char low = lorem[i] & 0xf;
    lorem[i] = low + lorem[i];
  }

  if (strcmp(lorem, argv[1]) == 0) goto success;
  else goto fail;
  /* ~\~ end */

 fail:
  printf("No, '%s' is not the correct password.\n", argv[1]);
  return 0;
 success:
  printf("Yes, '%s' is the correct password!\n", argv[1]);
  return 0;
}
/* ~\~ end */
