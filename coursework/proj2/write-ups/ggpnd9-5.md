---
title: ggpnd9-5
author: Marcus Simpkins
---

This Category II binary appears to be dealing with time. The main function is
basically just an alias for this function, `checker`:

```c
int checker(void)
{
  int retval;
  int hour;
  
  retval = current();
  if ((retval < 4) && (2 < retval)) {
    retval = fail();
  }
  else {
    retval = succeed();
  }
  return retval;
}
```

The `current` function is as follows:

```c
int current(void)
{
  int retval;
  int *errno;
  char *error_str;
  tm *timebuf;
  time_t rawtime;
  tm *current_time;
  
  time(&rawtime);
  errno = __errno_location();
  if (*errno == 0) {
    timebuf = localtime(&rawtime);
    errno = __errno_location();
    if (*errno == 0) {
      retval = timebuf->tm_hour;
    }
    else {
      errno = __errno_location();
      error_str = strerror(*errno);
      printf("ERROR function failed",error_str);
      retval = -1;
    }
  }
  else {
    errno = __errno_location();
    error_str = strerror(*errno);
    printf("ERROR function failed",error_str);
    retval = -1;
  }
  return retval;
}
```

As you can see, `current` returns -1 if something bad happens, but if all goes
well, it returns the current hour of the localtime. 

Let's circle back to `checker`:

```c
retval = current();
if ((retval < 4) && (2 < retval)) {
  retval = fail();
}
else {
  retval = succeed();
}
return retval;
```

So as we can see, the program fails when the hours (in 24 Hour format) was
between 2 and 4, exclusive. It succeeds when the time is outside of that range. 
This is the secret behavior of this program. 

We can verify this by testing! I couldn't find a nice command to change the
system time, so I just went into the system settings to change it. I run date
before each command to verify:

```shell
$ date && ./challenge-pool/ggpnd9_5
Mon Dec  7 03:48:05 EST 2020
fail
$ date && ./challenge-pool/ggpnd9_5
Mon Dec  7 16:48:37 EST 2020
pass
```

