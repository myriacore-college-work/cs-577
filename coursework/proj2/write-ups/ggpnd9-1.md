---
title: ggpnd9-1
author: Marcus Simpkins
---

\newcommand\ignore[1]{}

So I just threw this one in Ghidra for decompilation, and here's what we got:

```c
int main(int argc,char **argv)
{
  char c_idx;
  int retval;
  char c;
  int idx;
  char state;
  
  if (argc == 2) {
    state = '\0';
    idx = 0;
    while (argv[1][idx] != '\0') {
      c_idx = argv[1][idx];
                    /* WARNING: Could not find normalized switch variable to match jumptable */
      switch(state) {
      case '\0':
        if (c_idx == 'r') {
          state = '\x01';
        }
        break;
      case '\x04':
        if (c_idx == '0') {
          state = '\x05';
        }
        else {
          state = '\f';
        }
        break;
      case '\b':
        if (c_idx == 'k') {
          state = '\t';
        }
        else {
          if (c_idx != 'n') {
            state = '\f';
          }
        }
        break;
      default:
        state = '\f';
      }
      idx = idx + 1;
    }
    if (state == '\n') {
      succeed(argv[1]);
    }
    else {
      fail(argv[1]);
    }
    retval = 0;
  }
  else {
    printf("Exactly one argument required.");
    retval = -1;
  }
  return retval;
}
```

It's honestly quite a small program. So it's clear that the switch statement is
the most interesting thing here:

```c
state = '\0';
idx = 0;
while (argv[1][idx] != '\0') {
  c_idx = argv[1][idx];
                /* WARNING: Could not find normalized switch variable to match jumptable */
  switch(state) {
  case '\0':
    if (c_idx == 'r') {
      state = '\x01';
    }
    break;
  case '\x04':
    if (c_idx == '0') {
      state = '\x05';
    }
    else {
      state = '\f';
    }
    break;
  case '\b':
    if (c_idx == 'k') {
      state = '\t';
    }
    else {
      if (c_idx != 'n') {
        state = '\f';
      }
    }
    break;
  default:
    state = '\f';
  }
  idx = idx + 1;
}
if (state == '\n') {
  succeed(argv[1]);
}
else {
  fail(argv[1]);
}
```

Looking at this code, it almost looks like a regex engine. Here are the state
transition rules, laid out a bit more thoughtfully:

![](https://kroki.io/graphviz/svg/eNqlkE1PhDAQhu_8igl3Y3dTTwInj5684sZMYVg2y7ZkqIox-99lgUX5SI1ybOd92nmf9LBnLHN4gE8PgFEf0wNDCI9PXnOuXlU3ZqpsmwCgdE8xMpv3nDBtom9Eu_t2pE1KcZVjSWFZ4EFbqm0_yticOC5QUREGWBRgbE4MlUVLVbRrM9YMiSAz2kKGCYX-yWjjR89ZcHu5jLrw96NwE11I6FEf9QckOTImlthvvz-PuqjVRZRjTTVas9lMvWwccTuPb_8uQXUSmq-uGgJ_Th972I9-drmi2wHVxsICrsf4WKpcLVU6atdCTj1Jp9Za3M2Bf4iVnR3pFiuWxA7or2KFS6xYLVY4aoupJeGUulm2JLqqwuWIpyXP3hdBDGVC){ height=40% width=40% }

\ignore {

```graphviz
digraph D {
  rankdir = LR

  subgraph rest {
    edge[arrowhead = vee];
    node[shape=plaintext];
    fromr[label=<all other states>]
    tor[label=<<font face="mono">\f</font>>]
   
    fromr -> tor [label="any character"];
  }

  subgraph b {
    edge[arrowhead = vee];
    node[shape=plaintext];
    fromb[label=<<font face="mono">\b</font>>]
    tob_1[label=<<font face="mono">\t</font>>]
    tob_2[label=<<font face="mono">\f</font>>]
   
    fromb -> tob_1 [label=<"<font face="mono">k</font>">];
    fromb -> tob_2 [label=<not "<font face="mono">n</font>">];
  }

  subgraph 4 {
    edge[arrowhead = vee];
    node[shape=plaintext];
    from4[label=<<font face="mono">\x04</font>>]
    to4_1[label=<<font face="mono">\x05</font>>]
    to4_2[label=<<font face="mono">\f</font>>]
   
    from4 -> to4_1 [label=<"<font face="mono">0</font>">];
    from4 -> to4_2 [label=<not "<font face="mono">0</font>">];
  }

  subgraph 0 {
    edge[arrowhead = vee];
    node[shape=plaintext];
    from0[label=<<font face="mono">\0</font>>]
    to0[label=<<font face="mono">\x01</font>>]
   
    from0 -> to0 [label=<"<font face="mono">r</font>">];
  }
}
```

}

One dilemma we have here is that the success state is `\n`, and it doesn't
appear possible to reach a `\n` state, at least not according to this graph.

My only guess here is that this must've been a decompilation error on the part
of Ghidra, but I'm not really familiar enough with how switch statements compile
to be able to really tackle that. Plus, I honestly doubt that switch statements
compile in ways that could make ghidra break.

My conclusion in this case is that it the binary author might have messed up,
and accidentally built a lock without a key.
