---
title: 0DKFyl-2
author: Marcus Simpkins
---

After marking things up in ghidra, here's the decompiled C source code:

```c
int main(int argc,char **argv)
{
  char *input;
  int retval;
  int bangpos;
  int Ds;
  int Vs;
  int inputlen;
  int sum1;
  int sum2;
  
  if (argc == 2) {
    bangpos = 0;
    Ds = 0;
    Vs = 0;
    inputlen = 0;
    input = argv[1];
    while (input[inputlen] != '\0') {
      if (input[inputlen] == '!') {
        bangpos = bangpos + 1;
      }
      else {
        if (input[inputlen] == 'D') {
          Ds = Ds + 1;
        }
        else {
          if (input[inputlen] == 'v') {
            Vs = Vs + 1;
          }
        }
      }
      inputlen = inputlen + 1;
    }
    if (inputlen == 0x18) {
      if (((bangpos == 1) && (Ds == 2)) && (Vs == 1)) {
        sum1 = 0;
        sum2 = 0;
        while (bangpos < 6) {
          sum1 = sum1 + input[bangpos];
          sum2 = sum2 + input[0x18 - bangpos];
          bangpos = bangpos + 1;
        }
        if (sum2 == sum1) {
          puts("correct nice job.");
          retval = 0;
        }
        else {
          puts("incorrect string try again.");
          retval = 1;
        }
      }
      else {
        puts("incorrect string try again.");
        retval = 1;
      }
    }
    else {
      puts("incorrect string try again.");
      retval = 1;
    }
  }
  else {
    puts("needs exactly 1 argument.");
    retval = -1;
  }
  return retval;
}
```

So the first and easiest observation is that the length of the input needs to
be `0x18`, or decimal 24. 

Let's look at the first while loop:

```c
while (input[inputlen] != '\0') {
  if (input[inputlen] == '!') {
    bangpos = bangpos + 1;
  }
  else {
    if (input[inputlen] == 'D') {
      Ds = Ds + 1;
    }
    else {
      if (input[inputlen] == 'v') {
        Vs = Vs + 1;
      }
    }
  }
  inputlen = inputlen + 1;
}
```

This loop serves to initialize `inputlen`, and the `Ds` and `Vs` vars, which
track the number of `D` characters and `v` characters are in the input. 

Let's move onto the main if logic we really care about:

```c
if (((bangpos == 1) && (Ds == 2)) && (Vs == 1)) {
  sum1 = 0;
  sum2 = 0;
  while (bangpos < 6) {
    sum1 = sum1 + input[bangpos];
    sum2 = sum2 + input[0x18 - bangpos];
    bangpos = bangpos + 1;
  }
  if (sum2 == sum1) {
    puts("correct nice job.");
    retval = 0;
  }
  else {
    puts("incorrect string try again.");
    retval = 1;
  }
}
else {
  puts("incorrect string try again.");
  retval = 1;
}
```

Due to the first if-statement, we now know the password must have 2 `D`s, 1 `v`,
and a `!` as the second character.

Now, looking at the while loop, we can see that `sum1` tracks each character 
starting from the `!` - that is, each character starting from the second in the
input. So then `sum1` is just a sum of the ascii value of the input from the 
second to the 6th character. 

Let's figure out what `sum2`'s deal is, then. the index starts us off at the
`0x18`th character, and we actually sum downards towards the `0x18 - 6`th
character. 

With that in mind, here's a little diagram of our password:

\

```
  sum1              sum2
 +---+             +---+
_!______________________
```

The ascii value for `!` is 33, so let's say that the rest of the characters in 
the `sum1` range are just `A`s (whose ascii value is 65):

```
  sum1              sum2
 +---+             +---+
_!AAAA__________________
```

So, `sum2` has to sum to 293. After playing around a bit, I found that the 
sequence '`AAAB<space>`' should do it:

```
  sum1              sum2
 +---+             +---+
_!AAAA_____________AAAB 
```

We still need 2 'D's and 1 v, so we'll just sprinkle them around and call it a
day.

```
  sum1              sum2
 +---+             +---+
D!AAAADv___________AAAB 
```

So, our password is gonna be `D!AAAADv___________AAAB<space>`, which does work:

```shell
$ ./challenge-pool/0DKFyl_2 'D!AAAADv___________AAAB '
correct nice job.
```
