---
title: hy58ZR-6
author: Marcus Simpkins
---

The hidden behavior for this Category II binary is quite simple:

```c
int main(void)
{
  long lVar1;
  tm *timebuf;
  long in_FS_OFFSET;
  int result;
  time_t rawtime;
  tm *p;
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  time(&rawtime);
  timebuf = gmtime(&rawtime);
  printf("%d",(ulong)(uint)timebuf->tm_sec);
  if (lVar1 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return 0;
}
```

Here the program gets the system time, converts it into a [time
struct](https://linux.die.net/man/3/ctime) using
[`gmtime`](https://linux.die.net/man/3/gmtime), then prints the number of
seconds after the minute.

We can confirm this by testing it in the shell:

```shell
$ date +'%S' && ./hy58ZR_6 && echo ''
35
35
$ date +'%S' && ./hy58ZR_6 && echo ''
36
36
$ date +'%S' && ./hy58ZR_6 && echo ''
36
36
```
