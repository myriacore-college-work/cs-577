---
title: iC2P8s-1
author: Marcus Simpkins
---

This binary really intimidated me at first, because the footprint in the
decompiled source is quite large. However, I've been getting good at these
crackmes, so I solved it surprisingly quickly.

```c
undefined8 main(void)
{
  void **ppvVar1;
  undefined8 uVar2;
  long lVar3;
  undefined *puVar4;
  int iVar5;
  time_t tVar6;
  long __size;
  void *pvVar7;
  undefined *puVar8;
  long in_FS_OFFSET;
  undefined auStack136 [8];
  undefined *local_80;
  int local_70;
  int local_6c;
  int local_68;
  int local_64;
  int local_60;
  int local_5c;
  void *local_58;
  long local_50;
  undefined *local_48;
  long local_40;

  //... omitted variable initializations
  
  while (local_70 < local_64) {
    if (local_70 == local_60) {
      //... omitted some variable assignments
      *(undefined8 *)(puVar8 + lVar3 + -8) = 0x100b5b;
      printf("\nEnter input : ",0x10,(__size + 0xfU) % 0x10);
      //... omitted some variable assignments
    }
    else {
      //... ommitted some variable assignments and a malloc
    }
    local_70 = local_70 + 1;
  }
  // ... omitted more assignments
  local_5c = FUN_001009f6(uVar2);
  if (local_5c == 0x78) {
    *(undefined8 *)(puVar8 + -8) = 0x100c3e;
    puts("\nBingo!\n",puVar8[-8]);
  }
  else {
    *(undefined8 *)(puVar8 + -8) = 0x100c4c;
    puts("\nChallenge me if you can!\n",puVar8[-8]);
  }
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    *(undefined8 *)(puVar8 + -8) = 0x100c65;
    __stack_chk_fail();
  }
  return 0;
}
```

I had to omit a lot here because there really was a **lot** of stuff going on.
I didn't want to have to care, so I just looked for where the success printout
was, and it came directly after the result of calling `FUN_001009f6(uVar2)`. 

That function call looks promising, let's check that out:

```c
ulong FUN_001009f6(char *param_1)
{
  int iVar1;
  uint uVar2;
  ulong uVar3;
  
  if (*param_1 == '\0') {
    uVar3 = 0xe6;
  }
  else {
    iVar1 = strcmp(param_1,"AlasYouAreHere");
    if (iVar1 == 0) {
      uVar3 = 0xb4;
    }
    else {
      uVar2 = FUN_001008ca(param_1);
      uVar3 = (ulong)uVar2;
    }
  }
  return uVar3;
}
```

This `strcmp` looked interesting, but it's a red herring, since the outer `if`
statement in the main function wants the return value to be `0x78`. What's
*really* interesting is the call to this other function, `FUN_001008ca`. What's
*that* function do?

```c
ulong FUN_001008ca(char *param_1)
{
  uint uVar1;
  
  if (((((param_1[1] == 'i' && *param_1 == 'H') && (param_1[2] == 'p')) && (param_1[3] == 'H')) &&
      (((param_1[4] == 'i' && (param_1[5] == 'p')) &&
       ((param_1[6] == 'H' && ((param_1[7] == 'u' && (param_1[8] == 'r')))))))) &&
     ((param_1[9] == 'r' &&
      (((((param_1[10] == 'a' && (param_1[0xb] == 'y')) && (param_1[0xc] == '!')) &&
        ((param_1[0xd] == '!' && (param_1[0xe] == '!')))) && (param_1[0xf] == '\0')))))) {
    uVar1 = 0x78;
  }
  else {
    uVar1 = 100;
  }
  return (ulong)uVar1;
}
```

These are all `&&`'s, so it didn't take me very long to manually type out each
character to find what the compare is. The password is `HipHipHurray!!!`.

```shell
$ ./iC2P8s_1 

Enter input : HipHipHurray!!!

Bingo!

```

