---
title: ggpnd9-2
author: Marcus Simpkins
---

Alright, here's ghidra's decompiled C source for this binary:

```c
int main(int argc,char **argv)
{
  int retval;
  char *destination;
  char threebuff [20];
  char twobuff [20];
  char onebuff [20];
  char *str;
  int third;
  int second;
  int first;
  
  if (argc == 2) {
    destination = (char *)calloc(100,1);
    snprintf(onebuff,0x14,"%x",0x677d89);
    snprintf(twobuff,0x14,"%x",0x324b7b);
    snprintf(threebuff,0x14,"%x",0xfaf62b);
    my_strcat(destination,threebuff);
    my_strcat(destination,twobuff);
    my_strcat(destination,onebuff);
    retval = strcmp(argv[1],destination);
    if (retval == 0) {
      succeed(argv[1]);
      retval = 0;
    }
    else {
      fail(argv[1]);
      retval = -1;
    }
  }
  else {
    puts("Need exactly one argument.");
    retval = -1;
  }
  return retval;
}
```

These calls to `my_strcat` are interesting, but we don't *actually* need to see
what this function does, because there's a literal `strcmp` between
`destination` and `argv[1]`, and `destination` isn't based on `argv[1]`, so we
can just run on a garbage input in GDB and see what `destination` comes out as.

Ghidra says that the `strcmp` call is at address `0x555555555356`, so we'll just
set a breakpoint for that in GDB and see what the value of `destination` is.

```
(gdb) x/s $rsi
0x555555559260:	"faf62b324b7b677d89"
```

And as we can see, `faf62b324b7b677d89` is indeed correct:

```shell
$ ./ggpnd9_2 faf62b324b7b677d89
Yes, faf62b324b7b677d89 is correct!
```

