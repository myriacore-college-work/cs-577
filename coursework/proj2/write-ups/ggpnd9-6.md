---
title: ggpnd9-6
author: Marcus Simpkins
---

This binary is interesting, since it borrows the `current` function from
`ggpnd9-5`, but it changes the `success` and `checker` functions.

The changes made to `checker` were minimal, but noticeable:

```c
int checker(void)
{
  int retval;
  int hour;
  
  retval = current();
  if ((retval < 5) || (0xb < retval)) {
    retval = fail();
  }
  else {
    retval = succeed();
  }
  return retval;
}
```

As we can see, now the binary only succeeds if the hour (24 HR format) is
between 5 and 11. Before, it would succeed if the hours was 2 or below, or 4 or
above.

Here's the new `succeed` function:

```c
/* WARNING: Could not reconcile some variable overlaps */

void succeed(void)
{
  int rand1;
  int rand2;
  uint rand;
  FILE *tempfile;
  char bigbuff [120];
  FILE *fp;
  int euc;
  int q;
  int r;
  
  rand1 = rand();
  rand2 = rand();
  rand = gcd(rand1 % 100 + 1,rand2 % 100 + 1);
  bigbuff._0_8_ = 0;
  bigbuff._8_8_ = 0;
  bigbuff._16_8_ = 0;
  bigbuff._24_8_ = 0;
  bigbuff._32_8_ = 0;
  bigbuff._40_8_ = 0;
  bigbuff._48_8_ = 0;
  bigbuff._56_8_ = 0;
  bigbuff._64_8_ = 0;
  bigbuff._72_8_ = 0;
  bigbuff._80_8_ = 0;
  bigbuff._88_8_ = 0;
  bigbuff._96_8_ = 0;
  bigbuff._104_8_ = 0;
  bigbuff._112_8_ = 0;
  snprintf(bigbuff,0x78,"%x",(ulong)rand);
  tempfile = tmpfile();
  if (tempfile == (FILE *)0x0) {
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  fwrite(bigbuff,1,0x78,tempfile);
  fclose(tempfile);
                    /* WARNING: Subroutine does not return */
  exit(1);
}
```

Alright so there's definitely some compilation weirdness going on right now,
but the gist of this is that we're opening another tempfile, calculating a 
random number using `rand` and `gcd`, using `snprintf` to store the number into
`bugbuff` as a hex string, then writing that hex string to the tempfile. 

I think that the `exit` conditions might have been accidentally flipped, because
`exit(1)` usually means an error occurred, but here it's used in what we'd
consider a successful scenario.

Anyways, we can confirm this theory by calling `strace` with a fake system time:

```shell
$ date && echo "----------------------------" && strace ./challenge-pool/ggpnd9_6
Mon Dec  7 08:47:03 EST 2020
----------------------------
execve("./challenge-pool/ggpnd9_6", ["./challenge-pool/ggpnd9_6"], 0x7ffcf8b99c20 /* 27 vars */) = 0
/* ... Omitted */
openat(AT_FDCWD, "/usr/share/zoneinfo/EST", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=127, ...}) = 0
fstat(3, {st_mode=S_IFREG|0644, st_size=127, ...}) = 0
read(3, "TZif2\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\1\0\0\0\1\0\0\0\0"..., 4096) = 127
lseek(3, -71, SEEK_CUR)                 = 56
read(3, "TZif2\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\1\0\0\0\1\0\0\0\0"..., 4096) = 71
close(3)                                = 0
openat(AT_FDCWD, "/tmp", O_RDWR|O_EXCL|O_TMPFILE, 0600) = 3
fcntl(3, F_GETFL)                       = 0x418002 (flags O_RDWR|O_LARGEFILE|O_TMPFILE)
fstat(3, {st_mode=S_IFREG|0600, st_size=0, ...}) = 0
write(3, "3\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"..., 120) = 120
close(3)                                = 0
exit_group(1)                           = ?
+++ exited with 1 +++
```

Here you can see the program getting timezone info about my timezone, and then
you can see it openning the tempfile and writing to it. I guess that stuff with
`bugbuf` and the random numbers generated `3\0\0...`, cause that's what we're
writing back to the tempfile.
