---
title: c4y8c7_1
author: Marcus Simpkins
---

So this one hangs a bit when I try to launch it, and when I run `strings` on it,
I find this in the output:


```
$Info: This file is packed with the UPX executable packer http://upx.sf.net $
$Id: UPX 3.96 Copyright (C) 1996-2020 the UPX Team. All Rights Reserved. $
```

Turns out upx itself can unpack executables, which is pretty convenient. So, 
I can just kinda unpack it here:

```shell
$ upx -d c4y8c7_1.raw 
                       Ultimate Packer for eXecutables
                          Copyright (C) 1996 - 2020
UPX 3.96        Markus Oberhumer, Laszlo Molnar & John Reiser   Jan 23rd 2020

        File size         Ratio      Format      Name
   --------------------   ------   -----------   -----------
     14408 <-      7988   55.44%   linux/amd64   c4y8c7_1.raw

Unpacked 1 file.
```

Alright, here are our new function CFGs:

### `main`

![](cfg/main.svg)

### `mystery_basic_string`

![](cfg/mystery_basic_string.svg)


### New notes

- The only thing I could find on this `operator.delete` function was this
  [`operator delete` construct in C++](https://www.cplusplus.com/reference/new/operator%20delete/).
