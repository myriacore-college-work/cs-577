---
title: as1wRi-1
author: Marcus Simpkins
---

`as1wRi-1` appears to be a type-1 binary: 

```shell
$ ./as1wRi_1 
Exactly one argument is needed
```

Unfortunately, `strings` doesn't give me anything useful:

```shell
$ strings ./as1wRi_1 
...
GCC_3.0
GLIBC_2.4
GLIBC_2.2.5
GLIBCXX_3.4.21
CXXABI_1.3
GLIBCXX_3.4
u3UH
ATSH
@[A\]
[]A\A]A^A_
hello
9876543210
tooshort
Exactly one argument is needed
Yes, you cracked me!
No, nice try.
basic_string::_M_construct null not valid
;*3$"
zPLR
GCC: (GNU) 10.2.0
...
$ ./as1wRi_1 9876543210
No, nice try.
$ ./as1wRi_1 tooshort
No, nice try.
```

## Ghidra / Static Analysis

When I open this up in ghidra, it turns out there are quite a few functions at
play here:

```c
int main(int argc,char **argv)
{
  char cVar1;
  long in_FS_OFFSET;
  allocator<char> allocator;
  basic_string<char,std--char_traits<char>,std--allocator<char>> basic_string [40];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (argc != 2) {
    puts("Exactly one argument is needed");
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  allocator();
  mystery1(basic_string,argv[1],&allocator,argv[1]);
  cVar1 = mystery2(basic_string);
  ~basic_string(basic_string);
  ~allocator(&allocator);
  if (cVar1 == '\0') {
    sleep(1);
    puts("No, nice try.");
  }
  else {
    sleep(1);
    puts("Yes, you cracked me!");
  }
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return 0;
}

void mystery1(_Alloc_hider *basic_string,char *input,allocator *allocator,char *input2)
{
  long lVar1;
  char *c;
  
  c = (char *)_M_local_data();
  _Alloc_hider(basic_string,c,allocator);
  if (input == (char *)0x0) {
    c = (char *)0xffffffffffffffff;
  }
  else {
    lVar1 = mystery1_1(input);
    c = input + lVar1;
  }
  mystery_concat((char *)basic_string,input,c);
  return;
}

long mystery1_1(char *input)
{
  size_t sVar1;
  
  if (true) {
    sVar1 = strlen(input);
  }
  else {
    sVar1 = mystery1_1_1(input);
  }
  return sVar1;
}

/* WARNING: should be dead code??? */
long mystery1_1_1(char *input)
{
  char cVar1;
  long in_FS_OFFSET;
  undefined local_19;
  long local_18;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  local_18 = 0;
  while( true ) {
    local_19 = 0;
    cVar1 = mystery1_1_1_1(input + local_18,&local_19,input + local_18);
    if (cVar1 == '\x01') break;
    local_18 = local_18 + 1;
  }
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return local_18;
}

ulong same_location(char *c1,char *c2)
{
  return (ulong)(*c1 == *c2);
}

void thunk_mystery_construct(char *param_1,char *param_2,undefined8 param_3)
{
  mystery_construct(param_1,param_2,param_3,param_2);
  return;
}

void mystery_construct(char *param_1,char *param_2,forward_iterator_tag param_3)
{
  long lVar1;
  long in_FS_OFFSET;
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  _M_construct<char_const*>(param_1,param_2,param_3);
  if (lVar1 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}

undefined8 mystery2(ulong basic_string)
{
  long lVar1;
  long in_FS_OFFSET;
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  at(basic_string);
  if (lVar1 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return 0;
}
```

As you can see, there's quite a bit of garbage here. It's mainly due to the
sheer quantity of function calls. It doesn't become much easier when looking at
the CFG Diagrams:

#### `main`

![](cfg/main.svg)

#### `mystery1`

![](cfg/mystery1.svg)

#### `mystery1_1`

![](cfg/mystery1_1.svg)

#### `mystery1_1_1`

![](cfg/mystery1_1_1.svg)

#### `same_location`

![](cfg/same_location.svg)

#### `thunk_mystery_construct`

![](cfg/thunk_mystery_construct.svg)

#### `mystery_construct`

![](cfg/mystery_construct.svg)

#### `mystery2`

![](cfg/mystery2.svg)

---

My theory here is that this code was written in C++, since there's a lot of
weird stuff with `allocator`s, which [appear to be a C++
thing](http://www.cplusplus.com/reference/memory/allocator/). That might make
this quite difficult to debug, since C++ is *really* a high level language. 

The decompilation seems to be *super buggy*, so in general, the C source isn't
to be trusted here. What I should probably do from here is use GDB to get the
addresses of all of the functions we're seeing here, and then hopefully to see
which functions are called, why, and hopefully I can figure out what's going
on from there. 

Alright, so what does all this mean? How do we find the password? Well, my best
guess is that `mystery2` is very important, as it appears to be used to check
the return result. 

## Intermission - Python Script

I took a while to fix up the python script I've been using to generate the CFG
graphs up above. When doing so, I ran into some key insights. Including:

- You can ask ghidra to fill in thunks by right clicking on the thunk definition:
   
  ![](https://i.imgur.com/8glIq4i.png)
- C++ has a [`basic_string::at`](http://www.cplusplus.com/reference/string/basic_string/at/)
  function. This is what the thunk wants to fill out as when I try to set the 
  thunked function. It returns a "character reference" for the character at the
  given position. 
- I *think* that `_M_construct` is a string constructor. When it's called,
  I'm not super sure if it's doing concatenation, but it's definitely constructing
  a `basic_string` object. 
- `_M_local_data()` seems to be an implementation for the 
  [`basic_string::data`](https://www.cplusplus.com/reference/string/basic_string/data/)
  function. Not sure, though. 

## GDB - Dynamic Analysis

| Function         | Address in GDB   |
| ---------------- | ---------------- |
| Entrypoint       | `0x5555555561d0` |
| `main`           | `0x555555556465` |
| `mystery1`       | `0x5555555566bc` |
| `mystery2`       | `0x55555555630a` |
| `mystery1_1`     | `0x55555555656e` |
| `mystery_concat` | `0x555555556634` |
| `mystery1_1_1`   | `0x5555555565ca` |
| `mystery1_1_1_1` | `0x5555555565a8` |

A few notes:

- So, in `mystery1`, there's a function call to a function called `_Alloc_hider`.
  This function is located at `0x5555555560a0` in GDB, and gdb actually thinks
  it's called
  `<_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_Alloc_hiderC1EPcRKS3_@plt>`.
