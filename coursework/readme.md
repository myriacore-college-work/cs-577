---
title: Homework
---

This directory contains all the coursework I completed for this class! If you're
interested in the tools and tech I use, check the readmes in each directory.

| Homework Assignment      | Brief Description                                                 |
| ------------------------ | ----------------------------------------------------------------- |
| [Homework 1](hw1)        | 5 Crackme Password challenges                                     |
| [Project Phase 1](proj1) | Blue Team Challenge: Create 6 crackme binaries                    |
| [Project Phase 2](proj2) | Red Team Challenge: Reverse the blue team binaries made by others |
| [Homework 2](hw2)        | Memory Error Detection                                            |
